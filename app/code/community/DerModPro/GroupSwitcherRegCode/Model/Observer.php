<?php

class DerModPro_GroupSwitcherRegCode_Model_Observer
{
	public function customerSaveBefore(Varien_Event_Observer $observer)
	{
		$customer = $observer->getEvent()->getCustomer();
		if (! $customer->getId())
		{
			$customer->setGroupSwitcherRegCodeCheck(true);
		}
	}

	/**
	 * Return true if the rule matches
	 *
	 * @param Mage_Customer_Model_Customer $customer
	 * @param string $attributeCode
	 * @param DerModPro_GroupSwitcher_Model_Rule $rule
	 * @return bool
	 */
	public function applyRule(Mage_Customer_Model_Customer $customer, $attributeCode, DerModPro_GroupSwitcher_Model_Rule $rule)
	{
		if ($customer->getGroupSwitcherRegCodeCheck())
		{
			$code = (string) $customer->getGroupswitcherRegcode();
			if ($code !== '')
			{
				$model = Mage::getModel('GroupSwitcherRegCode/code')->loadByCode($code);
				if ($model->getId())
				{
					if ($model->getMaxUsages() == 0 || $model->getMaxUsages() >= $model->getUsageCount())
					{
						/*
						 * Set the target group id on the rule model
						 */
						$rule->setGroupIdAfter($model->getGroupId());
						return true;
					}
					else
					{
						/*
						 * Invalidate code:
						 */
						$customer->setGroupswitcherRegcode(
							Mage::helper('GroupSwitcherRegCode')->__('Maximum usage count exceeded: %s', $code)
						)
						->getResource()
						->saveAttribute($customer, 'groupswitcher_regcode');
					}
				}
			}
		}
		return false;
	}
}
