<?php
/**
 * Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcherRegCode
 * @copyright  Copyright (c) 2010 Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 * @license    http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html (DMCSL 1.0)
 */

class DerModPro_GroupSwitcherRegCode_Block_Adminhtml_Code_Edit_Tab_General extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();
		$this->setForm($form);

		if (Mage::getSingleton('adminhtml/session')->getFormData())
		{
			$formData =Mage::getSingleton('adminhtml/session')->getFormData();
			Mage::getSingleton('adminhtml/session')->setFormData(null);
		}
		elseif (Mage::registry('groupswitcher_code'))
		{
			$formData = Mage::registry('groupswitcher_code')->getData();
		}
		$fieldset = $form->addFieldset('code_form', array(
			'legend' => Mage::helper('GroupSwitcherRegCode')->__('Code Information')
		));

		$fieldset->addField('code', 'text', array(
			'label'     => Mage::helper('GroupSwitcherRegCode')->__('Code'),
			'class'     => 'required-entry',
			'required'  => true,
			'name'      => 'code',
		));

		/*
		$fieldset->addField('group_id', 'text', array(
			'label'     => Mage::helper('GroupSwitcherRegCode')->__('Customer Group'),
			'class'     => 'required-entry',
			'required'  => true,
			'name'      => 'group_id',
		));
		 */

		$fieldset->addField('group_id', 'select', array(
			'label'     => Mage::helper('GroupSwitcherRegCode')->__('Customer Group'),
			'required'  => true,
			'name'      => 'group_id',
			'values'    => $this->_getGroupOptions(),
			'value'     => isset($formData['group_id']) ? $formData['group_id'] : null
		));

		$fieldset->addField('max_usages', 'text', array(
			'label'     => Mage::helper('GroupSwitcherRegCode')->__('Max. Usages'),
			'class'     => '',
			'required'  => false,
			'name'      => 'max_usages',
			'note'      => Mage::helper('GroupSwitcherRegCode')->__('Set to zero for unlimited usages.'),
		));
		if (! isset($formData['max_usages']))
		{
			$formData['max_usages'] = 1;
		}

		$form->setValues($formData);

		return parent::_prepareForm();
	}

	protected function _getGroupOptions()
	{
		$groups = $this->getCustomerGroupHash();
		if (is_null($groups))
		{
			$groups = Mage::getResourceModel('customer/group_collection')
				->addFieldToFilter('customer_group_id', array('gt'=> 0))
				->load()
				->toOptionHash();
			$this->setCustomerGroupHash($groups);
		}
		$groups[''] = Mage::helper('GroupSwitcherRegCode')->__('-- Please Choose --');
		ksort($groups);
		return $groups;
	}
}