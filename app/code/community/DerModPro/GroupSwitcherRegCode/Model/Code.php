<?php

class DerModPro_GroupSwitcherRegCode_Model_Code extends Mage_Core_Model_Abstract
{
	protected $_eventPrefix = 'groupswitcher_code';

	protected $_eventObject = 'code';

	protected function _construct()
	{
		$this->_init('GroupSwitcherRegCode/code');
	}

	public function loadByCode($code)
	{
		$this->getResource()->loadByCode($this, $code);
		return $this;
	}

	public function getUsageCount()
	{
		return $this->getResource()->getUsageCount($this);
	}
}
