<?php
/**
 * Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcherRegCode
 * @copyright  Copyright (c) 2010 Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 * @license    http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html (DMCSL 1.0)
 */

class DerModPro_GroupSwitcherRegCode_Block_Adminhtml_Code extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_blockGroup = 'GroupSwitcherRegCode';
		$this->_controller = 'adminhtml_code';
		$this->_headerText = Mage::helper('GroupSwitcherRegCode')->__('Manage GroupSwitcher Codes');
		$this->_addButtonLabel = Mage::helper('GroupSwitcherRegCode')->__('Add New Code');
		parent::__construct();
	}
}

