<?php
/**
 * Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcherRegCode
 * @copyright  Copyright (c) 2010 Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 * @license    http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html (DMCSL 1.0)
 */

class DerModPro_GroupSwitcherregCode_Block_Adminhtml_Code_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		parent::__construct();

		$this->_objectId = 'id';
		$this->_blockGroup = 'GroupSwitcherRegCode';
		$this->_controller = 'adminhtml_code';
		$this->_mode = 'edit';

		$jsObjNamePrefix = 'code';

		$this->_updateButton('save', 'label', Mage::helper('GroupSwitcherRegCode')->__('Save Code'));
		$this->_updateButton('delete', 'label', Mage::helper('GroupSwitcherRegCode')->__('Delete Code'));

		$this->_addButton('save_and_continue', array(
				'label' => Mage::helper('adminhtml')->__('Save And Continue Edit'),
				'onclick' => 'saveAndContinueEdit()',
				'class' => 'save',
		), -100);

        $this->_formScripts[] = "
			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
    }

	public function getHeaderText()
	{
		if (Mage::registry('groupswitcher_code') && Mage::registry('groupswitcher_code')->getId())
		{
			return Mage::helper('GroupSwitcherRegCode')->__('Edit Code #%d', Mage::registry('groupswitcher_code')->getId());
		}
		else
		{
			return Mage::helper('GroupSwitcherRegCode')->__('New code');
		}
	}
}