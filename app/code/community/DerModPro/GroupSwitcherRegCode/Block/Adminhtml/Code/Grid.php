<?php
/**
 * Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcherRegCode
 * @copyright  Copyright (c) 2010 Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 * @license    http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html (DMCSL 1.0)
 */

class DerModPro_GroupSwitcherRegCode_Block_Adminhtml_Code_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('codeGrid');
		$this->setUseAjax(false);
		$this->setDefaultSort('id');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getResourceModel('GroupSwitcherRegCode/code_collection')
			->addUsageCount();

		$this->setCollection($collection);

		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{
		$this->addColumn('id', array(
			'header' => Mage::helper('GroupSwitcherRegCode')->__('ID'),
			'width' => '50px',
			'index' => 'id',
			'type' => 'number',
		));
		$this->addColumn('code', array(
			'header' => Mage::helper('GroupSwitcherRegCode')->__('Code'),
			'index' => 'code',
		));
		$this->addColumn('group_id', array(
			'header' => Mage::helper('GroupSwitcherRegCode')->__('Group'),
			'index' => 'group_id',
			'type' => 'options',
			'options' => $this->_getGroupOptions(),
		));
		$this->addColumn('max_usages', array(
			'header' => Mage::helper('GroupSwitcherRegCode')->__('Max. Usages'),
			'index' => 'max_usages',
			'type' => 'number',
		));
		$this->addColumn('usage_count', array(
			'header' => Mage::helper('GroupSwitcherRegCode')->__('# of Uses'),
			'index' => 'usage_count',
			'type' => 'number',
		));

		$this->addColumn('action', array(
			'header' => Mage::helper('GroupSwitcherRegCode')->__('Action'),
			'width' => '100',
			'type' => 'action',
			'getter' => 'getId',
			'actions' => array(
				array(
					'caption' => Mage::helper('GroupSwitcherRegCode')->__('Edit'),
					'url' => array('base' => '*/*/edit'),
					'field' => 'id'
				)
			),
			'filter' => false,
			'sortable' => false,
			'index' => 'stores',
			'is_system' => true,
		));
		
		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}

	protected function _getGroupOptions()
	{
		$groups = $this->getCustomerGroupHash();
		if (is_null($groups))
		{
			$groups = Mage::getResourceModel('customer/group_collection')
				->addFieldToFilter('customer_group_id', array('gt'=> 0))
				->load()
				->toOptionHash();
			$this->setCustomerGroupHash($groups);
		}
		ksort($groups);
		return $groups;
	}
}

