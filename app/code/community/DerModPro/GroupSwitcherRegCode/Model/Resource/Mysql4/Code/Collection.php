<?php

class DerModPro_GroupSwitcherRegCode_Model_Resource_Mysql4_Code_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	protected function  _construct()
	{
		$this->_init('GroupSwitcherRegCode/code');
	}

	public function addUsageCount()
	{
		$attribute = Mage::getModel('customer/attribute');
		/* @var $attribute Mage_Customer_Model_Attribute */
		$attribute->loadByCode('customer', 'groupswitcher_regcode');


        $select = $this->getConnection()->select()
            ->from(array('used_codes' => $attribute->getBackendTable()), array(
				'code' => 'value',
                'usage_count' => new Zend_Db_Expr('COUNT(*)'),
            ))
			->where('attribute_id=?', $attribute->getId())
			->where('value IS NOT NULL')
            ->group('value');

        $this->getSelect()->joinLeft(
            array('usage_count' => $select),
            'main_table.code = usage_count.code',
            array('usage_count' => new Zend_Db_Expr('IFNULL(usage_count, 0)'))
        );

		return $this;
	}
}
