<?php
/**
 * Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcherRegCode
 * @copyright  Copyright (c) 2010 Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 * @license    http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html (DMCSL 1.0)
 */

class DerModPro_GroupSwitcherRegCode_Block_Adminhtml_Code_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('code');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('GroupSwitcherRegCode')->__('Code Setup'));
	}

	protected function _beforeToHtml()
	{
		$this->addTab('general_section', array(
				'label'   => Mage::helper('GroupSwitcherRegCode')->__('General'),
				'title'   => Mage::helper('GroupSwitcherRegCode')->__('General'),
				'content' => $this->getLayout()->createBlock('GroupSwitcherRegCode/adminhtml_code_edit_tab_general')->toHtml(),
		));

		return parent::_beforeToHtml();
	}
}