<?php
/**
 * Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the
 * Der Modulprogrammierer - COMMERCIAL SOFTWARE LICENSE (v1.0) (DMCSL 1.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html
 *
 *
 * @category   DerModPro
 * @package    DerModPro_GroupSwitcherRegCode
 * @copyright  Copyright (c) 2010 Der Modulprogrammierer - Vinai Kopp, Rico Neitzel GbR
 * @license    http://www.der-modulprogrammierer.de/licenses/dmcsl-1.0.html (DMCSL 1.0)
 */

/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS `{$installer->getTable('GroupSwitcherRegCode/reg_codes')}`;
CREATE TABLE `{$installer->getTable('GroupSwitcherRegCode/reg_codes')}` (
	`id` INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`code` VARCHAR(255) NOT NULL,
	`group_id` SMALLINT(3) UNSIGNED NOT NULL,
	`max_usages` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 1,
	CONSTRAINT `FK_gsregcode_group_id` FOREIGN KEY (`group_id`) REFERENCES `{$installer->getTable('customer_group')}` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->addAttribute('customer', 'groupswitcher_regcode', array(
	'type' => 'varchar',
	'label' => 'GroupSwitcher Registration Code',
	'frontend' => 'GroupSwitcher/entity_attribute_frontend_translate_label',
	'required' => false,
	'visible' => true,
));

$installer->endSetup();